export interface IStrategyAction<T extends string> {
  id: string;
  type: T;
}

export type IWaitForRate = IStrategyAction<"WAIT_FOR_RATE">;

export type IBuy = IStrategyAction<"BUY">;

export type ISell = IStrategyAction<"SELL">;

export type IAnyAction = IWaitForRate | IBuy | ISell;
