import { IStrategyAction } from "./strategyAction";

export interface IStrategy {
  actions: Array<IStrategyAction<any>>;
}
