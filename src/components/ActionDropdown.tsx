import * as React from "react";
import * as uuid from "uuid/v4";
import {
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "reactstrap";
import { IAnyAction, IStrategyAction } from "src/contracts/strategyAction";

interface IActionDropdownProps {
  onActionCreated(action: IStrategyAction<any>): void;
}
interface IActionDropdownState {
  dropdownOpen: boolean;
}

export default class ActionDropdown extends React.Component<
  IActionDropdownProps,
  IActionDropdownState
> {
  constructor(props: IActionDropdownProps) {
    super(props);
    this.state = {
      dropdownOpen: false
    };
  }

  public toggle = () => {
    this.setState(({ dropdownOpen }) => ({ dropdownOpen: !dropdownOpen }));
  };

  public createAction = (type: IAnyAction["type"]) => (
    event: React.MouseEvent<any>
  ) => {
    const id = uuid();
    this.props.onActionCreated({ id, type });
  };

  public render() {
    return (
      <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret={true}>Add action...</DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={this.createAction("WAIT_FOR_RATE")}>
            Wait for Rate
          </DropdownItem>
          <DropdownItem onClick={this.createAction("BUY")}>Buy</DropdownItem>
          <DropdownItem onClick={this.createAction("SELL")}>Sell</DropdownItem>
          <DropdownItem divider={true} />
          <DropdownItem>Fork</DropdownItem>
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}
