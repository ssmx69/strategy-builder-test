import * as React from "react";
import { ISell } from "src/contracts/strategyAction";
import { Alert } from "reactstrap";

interface ISellActionProps {
  action: ISell;
  onDeleteActionClicked: () => void;
}

const SellAction: React.StatelessComponent<ISellActionProps> = ({
  onDeleteActionClicked
}) => {
  return (
    <Alert color="danger" isOpen={true} toggle={onDeleteActionClicked}>
      Sell
    </Alert>
  );
};

export default SellAction;
