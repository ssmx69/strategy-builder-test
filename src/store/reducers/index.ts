import { combineReducers } from "redux";
import strategyReducer from "./strategyReducer";

const rootReducer = combineReducers({
  strategy: strategyReducer
});

export default rootReducer;
