import {
  createAddAction,
  createDeleteAction
} from "../actions/strategyActions";
import { createInitialStrategyState } from "../state/strategyState";
import strategyReducer from "./strategyReducer";

test("Should initialize state with empty strategy", () => {
  const newState = strategyReducer(undefined, {} as any);
  expect(newState).toEqual(createInitialStrategyState());
});

test("Should add new action to the end of strategy", () => {
  const newAction = {
    id: "new-action-id",
    payload: {},
    type: "action-type"
  };

  const newState = strategyReducer(undefined, createAddAction(newAction));

  expect(newState).toEqual({
    actions: [newAction]
  });
});

test("Should delete action from strategy by id", () => {
  const state = {
    actions: [
      {
        id: "new-action-1",
        payload: {},
        type: "action-type"
      },
      {
        id: "new-action-2",
        payload: {},
        type: "action-type"
      },
      {
        id: "new-action-3",
        payload: {},
        type: "action-type"
      }
    ]
  };

  const newState = strategyReducer(state, createDeleteAction("new-action-2"));

  expect(newState).toEqual({
    actions: [
      {
        id: "new-action-1",
        payload: {},
        type: "action-type"
      },
      {
        id: "new-action-3",
        payload: {},
        type: "action-type"
      }
    ]
  });
});
