import { IStrategy } from "src/contracts/strategy";

export const createInitialStrategyState = (): IStrategy => ({
  actions: []
});
