import { IStrategyAction } from "src/contracts/strategyAction";
import { createAction } from "./action";

export const ADD_ACTION = "ADD_ACTION";
export const DELETE_ACTION = "DELETE_ACTION";

export const createAddAction = (action: IStrategyAction<any>) =>
  createAction(ADD_ACTION, { action });
export type IAddAction = ReturnType<typeof createAddAction>;

export const createDeleteAction = (id: IStrategyAction<any>["id"]) =>
  createAction(DELETE_ACTION, { id });
export type IDeleteAction = ReturnType<typeof createDeleteAction>;
