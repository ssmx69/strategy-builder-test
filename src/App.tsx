import * as React from "react";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import { IAppState } from "./store/state/appState";
import { IStrategyAction } from "./contracts/strategyAction";
import {
  createAddAction,
  IAddAction,
  createDeleteAction,
  IDeleteAction
} from "./store/actions/strategyActions";
import Strategy from "./components/Strategy";
import "./App.css";

const App: React.StatelessComponent<
  ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>
> = props => {
  return (
    <div className="App">
      <Strategy
        onDeleteActionClicked={props.handleDeleteAction}
        onCreateActionClicked={props.handleCreateAction}
        strategy={props.state.strategy}
      />
    </div>
  );
};

export const mapStateToProps = ({ strategy }: IAppState, ownProps: {}) => ({
  state: { strategy },
  ...ownProps
});

export const mapDispatchToProps = (
  dispatch: Dispatch<IAddAction | IDeleteAction>
) => ({
  handleCreateAction: (action: IStrategyAction<any>) => {
    dispatch(createAddAction(action));
  },
  handleDeleteAction: (id: IStrategyAction<any>["id"]) => () => {
    dispatch(createDeleteAction(id));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
