import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { compose, createStore } from "redux";
import App from "./App";
import rootReducer from "./store/reducers";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";

const composeEnhancers =
  // tslint:disable-next-line:no-string-literal
  window["__REDUX_DEVTOOLS_EXTENSION_COMPOSE__"] || compose;
const store = createStore(rootReducer, composeEnhancers());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root") as HTMLElement
);
